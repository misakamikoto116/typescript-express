import { Request, Response } from 'express';
import Authentication from '../utils/Authentication';
import { compare } from 'bcrypt';
const db = require("../db/models");

class AuthController {
  register = async (req: Request, res: Response): Promise<Response> => {
    let { username, password } = req.body;

    const hashedPassword: string = await Authentication.passwordHash(password);

    await db.user.create({username, password: hashedPassword});

    return res.send("Berhasil Di Registrasi");
  }

  login = async (req: Request, res: Response): Promise<Response> => {
    let { username, password } = req.body;

    // Cek Username
    const user = await db.user.findOne({
      where: { username }
    });

    // cek password
    if (user) {
      let compare = await Authentication.passwordCompare(password, user.password);

      // generate token
      if (compare) {
        let token = Authentication.generateToken(user.id, username, user.password);
        
        return res.send({
          token: token
        });
      }

      return res.send("Wrong Password!")
    }

    return res.send("User Not Found!");
  }

  // Hasilnya jadi data karena di generate token / AuthValidator.ts yang dimasukkan id, username, password
  profile = (req: Request, res: Response): Response => {
    return res.send(req.app.locals.credential);
  }

}

export default new AuthController();